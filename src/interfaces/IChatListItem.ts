export default interface IChatListItem {
  id: number;
  subject: string;
  created: Date;
  preview: string;
}
