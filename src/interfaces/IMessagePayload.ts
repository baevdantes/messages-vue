import IUser from "@/interfaces/IUser";

export default interface IMessagePayload {
  text: string;
  user: IUser;
  chatId: number;
}
