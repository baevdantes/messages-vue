import IUser from "@/interfaces/IUser";

export default interface IMessage {
  id: number;
  user: IUser;
  created: Date;
  text: string;
}

export interface IMessageForAdding {
  user: IUser;
  created: Date;
  text: string;
  chatId: number;
}
