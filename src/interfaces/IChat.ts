import IMessage from "@/interfaces/IMessage";

export default interface IChat {
  id: number;
  subject: string;
  created: Date;
  messages: IMessage[];
}

