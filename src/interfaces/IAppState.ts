import IChat from "@/interfaces/IChat";
import IChatListItem from "@/interfaces/IChatListItem";
import IUser from "@/interfaces/IUser";

export default interface IAppState {
  chatsList: IChatListItem[];
  currentChat: IChat;
  currentUser: IUser;
  chats: IChat[];
  users: IUser[];
  isLoading: boolean;
  messageText: string;
}
