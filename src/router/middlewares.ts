import store from "@/store";
import {getChat} from "@/store/actions";
import router from "@/router/index";
import {Route} from "vue-router";
import {setLoader} from "@/store/mutations";

export default class Middlewares {
  static fetchChat(to: Route, from: Route, next: () => void) {
    store.commit(setLoader, true);
    store.dispatch(getChat, Number(to.params.id)).then(() => {
      next();
    }).catch(() => {
      router.push("/404").then();
    }).finally(() => {
      store.commit(setLoader, false);
    });
  }
}
