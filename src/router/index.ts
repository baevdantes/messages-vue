import Vue from "vue";
import VueRouter, {Route, RouteConfig} from "vue-router";
import store from "@/store";
import {getChat, getChatsList} from "@/store/actions";
import Middlewares from "@/router/middlewares";

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: "/",
    name: "Home",
    component: () => import( "@/views/empty.vue"),
    beforeEnter: (to: Route, from: Route, next: () => void) => {
      store.dispatch(getChat, 1).then(() => {
        router.push("/chat/1").then();
      }).catch(() => {
        next();
      });
    },
  },
  {
    path: "/chat/:id",
    name: "Chat",
    component: () => import( "@/views/chat/chat.vue"),
    beforeEnter: (to: Route, from: Route, next: () => void) => {
      Middlewares.fetchChat(to, from, next);
    },
  },
  {
    path: "*",
    name: "404",
    component: () => import("@/views/404.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  store.dispatch(getChatsList).then(() => {
    next();
  }).catch(() => {
    next();
  });
});

export default router;
