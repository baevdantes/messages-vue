import {Component, Prop, Vue, Watch} from "vue-property-decorator";
import btn from "@/components/btn.vue";
import letter from "@/components/letter.vue";
import {isNotEmpty} from "@/utils";

@Component({
  components: {
    btn,
    letter,
  },
})
export default class TextField extends Vue {
  @Prop() isLoading: boolean;
  @Prop() text: string;

  addMessage() {
    if (this.isValidText) {
      this.$emit("addMessage", this.text);
    }
  }

  get isValidText() {
    return isNotEmpty(this.text);
  }
}
