import {Component, Prop, Vue} from "vue-property-decorator";
import ChatListHeader from "./components/chats-list__header.vue";
import IChatListItem from "@/interfaces/IChatListItem";
import ChaLink from "./components/chat-link.vue";

@Component({
  components: {
    ChatListHeader,
    ChaLink,
  }
})
export default class ChatsList extends Vue {
  @Prop() chats: IChatListItem[];
}
