export function isNotEmpty(value: string) {
  return value ? value.length && !/^\s+$/.test(value) : false;
}
