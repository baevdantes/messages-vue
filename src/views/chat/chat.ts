import {Component, Vue} from "vue-property-decorator";
import IChat from "@/interfaces/IChat";
import {Action, State} from "vuex-class";
import {Route} from "vue-router";
import Middlewares from "@/router/middlewares";
import TextField from "@/components/textfield/textfield.vue";
import IAppState from "@/interfaces/IAppState";
import {addMessage, updateMessageText} from "@/store/actions";
import {IMessageForAdding} from "@/interfaces/IMessage";
import IUser from "@/interfaces/IUser";
import message from "./components/message.vue";

@Component({
  beforeRouteUpdate: (to: Route, from: Route, next: () => void) => {
    Middlewares.fetchChat(to, from, next);
  },
  components: {
    TextField,
    message,
  },
})
export default class Chat extends Vue {
  @State((state: IAppState) => state.currentChat) chat: IChat;
  @State((state: IAppState) => state.currentUser) user: IUser;
  @State((state: IAppState) => state.messageText) text: string;
  @Action(addMessage) addMessageAction: (message: IMessageForAdding) => Promise<void>;
  @Action(updateMessageText) updateMessageText: (text: string) => Promise<void>;

  isLoading: boolean = false;

  addMessage(text: string) {
    this.isLoading = true;
    this.addMessageAction({
      created: new Date(),
      user: this.user,
      text,
      chatId: this.chat.id,
    }).finally(() => {
      this.isLoading = false;
      const chatEl: Element = document.querySelector(".chat__messages");
      chatEl.scrollTo({
        top: chatEl.scrollHeight,
      });
    });
  }
}
