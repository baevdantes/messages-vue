import Vue from "vue";
import app from "./app.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import "./main.scss";
import {EnumMonths} from "@/interfaces/EnumMonths";

Vue.config.productionTip = false;

Vue.filter("date", (date: Date): string => {
  return [date.getDate(), EnumMonths[date.getMonth()], date.getFullYear()].join(" ");
});

Vue.filter("dateMessage", (date: Date): string => {
  const monthNumber: string = (date.getMonth() + 1).toString().length === 1
    ? "0".concat((date.getMonth() + 1).toString())
    : (date.getMonth() + 1).toString();
  return [date.getDate(), monthNumber, date.getFullYear()].join(".").concat(" ")
    .concat((date.getHours()).toString()).concat(":").concat((date.getMinutes()).toString());
});

new Vue({
  router,
  store,
  render: (h) => h(app),
}).$mount("#app");
