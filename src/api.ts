import IMessagePayload from "@/interfaces/IMessagePayload";
import IChatListItem from "@/interfaces/IChatListItem";
import IChat from "@/interfaces/IChat";
import mockChats from "@/store/mock/chats.mock";

export default class Api {
  static getChatsList(): IChatListItem[] {
    return [];
  }

  static getChat(id: number): IChat {
    return mockChats.find((chat: IChat) => chat.id === id);
  }

  static addMessage(payload: IMessagePayload) {

  }
}
