import {Component, Vue} from "vue-property-decorator";
import ChatsList from "@/components/chats-list/chats-list.vue";
import {State} from "vuex-class";
import IChatListItem from "@/interfaces/IChatListItem";
import loader from "@/components/loader.vue";
import IAppState from "@/interfaces/IAppState";

@Component({
  components: {
    ChatsList,
    loader,
  }
})
export default class App extends Vue {
  @State((state: IAppState) => state.chatsList) chatsList: IChatListItem[];
  @State((state: IAppState) => state.isLoading) isLoading: boolean;
}
