import {ActionTree} from "vuex";
import IAppState from "@/interfaces/IAppState";
import IChatListItem from "@/interfaces/IChatListItem";
import IChat from "@/interfaces/IChat";
import {setChat, setChatsList, setMessageText, setNewMessage} from "@/store/mutations";
import IMessage, {IMessageForAdding} from "@/interfaces/IMessage";

export const getChatsList = "getChatsList";
export const getChat = "getChat";
export const addMessage = "addMessage";
export const updateMessageText = "updateMessageText";
export const maxLetters: number = 85;

const actions: ActionTree<IAppState, IAppState> = {
  [getChatsList]: ({state, commit}): Promise<IChatListItem[]> => {
    return new Promise((resolve, reject) => {
      const list: IChatListItem[] = state.chats.map((chat: IChat) => {
        const preview: string = chat.messages && chat.messages.length ? chat.messages[0].text : "";
        const splicedPreview: string = preview.length >= maxLetters
          ? preview.split("").splice(0, maxLetters).concat(["..."]).join("")
          : preview;
        return {
          created: chat.created,
          id: chat.id,
          preview: splicedPreview,
          subject: chat.subject,
        };
      });
      if (list && list.length) {
        commit(setChatsList, list);
        resolve(list);
      } else {
        reject(new Error().message);
      }
    });
  },

  [addMessage]: ({state, commit}, payload: IMessageForAdding) => {
    const foundChat: IChat = state.chats.find((chat: IChat) => chat.id === payload.chatId);
    const newMessage: IMessage = {
      text: payload.text,
      user: payload.user,
      id: foundChat.messages.length + 1,
      created: payload.created,
    };
    return new Promise((resolve, reject) => {
      if (foundChat) {
        setTimeout(() => {
          commit(setNewMessage, {newMessage, foundChat});
          resolve(newMessage);
        }, 500);
      } else {
        setTimeout(() => {
          reject(new Error().message);
        }, 500);
      }
      commit(setMessageText, "");
    });
  },

  [getChat]: ({state, commit}, id: number) => {
    commit(setMessageText, "");
    return new Promise((resolve, reject) => {
      const foundChat: IChat = state.chats.find((chat: IChat) => chat.id === id);
      if (foundChat) {
        setTimeout(() => {
          commit(setChat, foundChat);
          resolve(foundChat);
        }, 1000);
      } else {
        setTimeout(() => {
          reject(new Error().message);
        }, 1000);
      }
    });
  },

  [updateMessageText]: ({commit}, event: any) => {
    commit(setMessageText, event.target.value);
  },
};

export default actions;
