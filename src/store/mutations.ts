import {MutationTree} from "vuex";
import IAppState from "@/interfaces/IAppState";
import IChatListItem from "@/interfaces/IChatListItem";
import IChat from "@/interfaces/IChat";
import IMessage, {IMessageForAdding} from "@/interfaces/IMessage";

export const setChatsList = "setChatsList";
export const setChat = "setChat";
export const setNewMessage = "setNewMessage";
export const setLoader = "setLoader";
export const setMessageText = "setMessageText";

const mutations: MutationTree<IAppState> = {
  [setChatsList]: (state, payload: IChatListItem[]) => {
    state.chatsList = payload;
  },

  [setChat]: (state, payload: IChat) => {
    state.currentChat = payload;
  },

  [setNewMessage]: (state, payload: {newMessage: IMessage, foundChat: IChat}) => {
    payload.foundChat.messages.push(payload.newMessage);
  },

  [setLoader]: (state, flag: boolean) => {
    state.isLoading = flag;
  },

  [setMessageText]: (state, text: string) => {
    state.messageText = text;
  },
};

export default mutations;
