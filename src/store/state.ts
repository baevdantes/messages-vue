import IAppState from "@/interfaces/IAppState";
import mockUsers from "@/store/mock/users.mock";
import mockChats from "@/store/mock/chats.mock";

export const currentChat = "currentChat";
export const chatsList = "chatsList";
export const chats = "chats";
export const users = "users";
export const currentUser = "currentUser";
export const isLoading = "isLoading";
export const messageText = "messageText";

const state: IAppState = {
  [chatsList]: [],
  [chats]: mockChats,
  [users]: mockUsers,
  [currentChat]: null,
  [currentUser]: mockUsers[0],
  [isLoading]: false,
  [messageText]: "",
};

export default state;
