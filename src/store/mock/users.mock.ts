import IUser from "@/interfaces/IUser";

const mockUsers: IUser[] = [
  {
    id: 1,
    name: "Даниил",
  },
  {
    id: 2,
    name: "Сергей",
  },
];

export default mockUsers;
