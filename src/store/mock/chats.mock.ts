import IChat from "@/interfaces/IChat";
import mockUsers from "@/store/mock/users.mock";

const mockChats: IChat[] = [
  {
    id: 1,
    subject: "Тема 1",
    created: new Date(2019, 10, 25, 12, 25, 0),
    messages: [
      {
        created: new Date(2019, 10, 25, 18, 25, 0),
        id: 1,
        text: "Morbi hac eligendi totam? Elementum mi facilis aliquet id turpis ultrices mollitia porttitor " +
          "praesentium animi ullamco eleifend scelerisque, sapien quam explicabo conubia egestas vehicula!",
        user: mockUsers[0],
      },
      {
        created: new Date(2019, 10, 25, 18, 35, 0),
        id: 2,
        text: "Здравствуй!",
        user: mockUsers[1],
      },
      {
        created: new Date(2019, 10, 25, 18, 40, 0),
        id: 3,
        text: "Пока!",
        user: mockUsers[1],
      },
    ]
  },
  {
    id: 2,
    subject: "Тема 2",
    created: new Date(2019, 0, 25, 12, 25, 0),
    messages: [
      {
        created: new Date(2019, 0, 25, 18, 25, 0),
        id: 1,
        text: "Привет!",
        user: mockUsers[0],
      },
      {
        created: new Date(2019, 0, 25, 18, 35, 0),
        id: 2,
        text: "Привет и тебе!",
        user: mockUsers[1],
      },
      {
        created: new Date(2019, 0, 25, 18, 40, 0),
        id: 3,
        text: "Хорошего дня!",
        user: mockUsers[1],
      },
      {
        created: new Date(2019, 10, 25, 18, 40, 0),
        id: 4,
        text: "И тебе, спасибо!",
        user: mockUsers[0],
      },
    ]
  },
];

export default mockChats;
